import React from 'react';
import PropTypes from 'prop-types';

import KendoGrid from '../KendoGrid/KendoGrid';
import { GridColumn as CustomeColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-default/dist/all.css';
// import products from '../KendoGrid/product.json';
import products from '../../src/KendoGrid/products.json';
import GridComponent from './GridComponent';
import { process } from '@progress/kendo-data-query';

const gridColumn = [
  {
    FieldName: 'ProductID',
    ColumnHeader: 'ID',
    ColumnWidth: '40px',
    FlterType: 'numeric',
    CoumnFilter: 'ColumnMenu',
    IsCustomColumn: false,
    Filterable: false
  },
  {
    FieldName: 'ProductName',
    ColumnHeader: 'Name',
    ColumnWidth: '250px',
    FilterType: 'text', //date , boolean
    CoumnFilter: 'ColumnMenuCheckboxFilter',
    IsCustomColumn: false,
    Filterable: true
  }
];

const chkChangeHandler = (e, props) => {
  alert('check box handler');
  console.log(e.target);
  console.log(props);
};
const defaultDataState = { takeRows: 10, skipRows: 0 };
const kendoProp = {
  gridColumn: gridColumn,
  gridClass: 'gridClass',
  dataState: defaultDataState,
  products: process(products.slice(), defaultDataState),
  total: products.length,
  sortable: true,
  pageable: true,
  keyColumn: 'ProductID'
};
const changeEventHandler = event => {
  console.log(event.data);
  console.log(process(products, event.data));
};
const GridConsumer = props => {
  return (
    <div className='App'>
      <GridComponent
        {...kendoProp}
        dataList={{
          result: { data: products, total: 77 },
          dataState: { take: 10, skip: 0 }
        }}
        changeEventHandler={changeEventHandler}
      ></GridComponent>
    </div>
  );
};

GridConsumer.propTypes = {};

export default GridConsumer;
