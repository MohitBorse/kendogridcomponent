import React from 'react';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import { ColumnMenu } from './gridColumnHeaderFilters.js';
import './gridCustomCss.scss';
import { filterBy } from '@progress/kendo-data-query';
import { GridColumnMenuCheck } from './gridColumnHeaderFilters';
const GridComponent = ({
  columns, // column
  children, // custom column
  gridClass, //css
  rows, // CollectionData
  sortable, // sortable
  pageable, //pageable
  filterable, //grid level fiter
  dataCollection,
  changeEventHandler
}) => {
  let columncheck = null;
  let rowsCollections = [...dataCollection];

  const getColumnFilter = name => {
    switch (name) {
      case 'ColumnMenu':
        return ColumnMenu;
        break;
      case 'ColumnMenuCheckboxFilter':
        return GridColumnMenuCheck(rowsCollections);
        break;
      default:
        break;
    }
  };

  const generateColumns = () => {
    return columns.map((item, key) => {
      if (!item.customColumn) {
        return (
          <Column
            key={item}
            {...item.props}
            columnMenu={getColumnFilter(item.columnMenu)}
          />
        );
      } else {
        //   <td dangerouslySetInnerHTML={{ __html: item.cell }}></td>
        return (
          <Column
            key={item}
            {...item.props}
            columnMenu={getColumnFilter(item.columnMenu)}
            cell={props => (
              <td>
                {/* <input type='checkbox' checked={props.dataItem[props.field]} /> */}
                {generateHtmlElement(item.element, props, item)}
              </td>
            )}
          />
        );
      }
    });
  };

  const generateHtmlElement = (key, props, item) => {
    debugger;
    let functItem = item.func;
    switch (key) {
      case 'checkbox':
        return (
          <input
            type='checkbox'
            disabled={item.disabled}
            checked={props.dataItem[props.field]}
            onChange={e => item.events[0](e, props.dataItem)}
          />
        );
        break;
      case 'button2':
        return (
          <React.Fragment>
            <button
              className={item.cssStyle}
              disabled={item.disabled}
              onClick={e => item.events[0](e, props.dataItem)}
            >
              {item.btnTitle[0]}
            </button>
            <button
              className={item.cssStyle}
              disabled={item.disabled}
              style={{ marginLeft: '8pt' }}
              onClick={e => item.events[0](e, props.dataItem)}
            >
              {item.btnTitle[1]}
            </button>
          </React.Fragment>
        );
        break;
      case 'textlink':
        return (
          <label
            style={{ textDecoration: 'underline', color: 'blue' }}
            disabled={item.disabled}
            onClick={item.events[0]}
          >
            {props.dataItem[props.field]}
          </label>
        );
        break;

      default:
        break;
    }
  };

  return (
    <div>
      <Grid
        className={gridClass}
        data={rows.result} // {result,total}
        {...rows.dataState}
        onDataStateChange={e => changeEventHandler(e)}
        sortable={sortable}
        filterable={filterable}
        pageable={pageable}
        resizable={true} //Not working
      >
        {generateColumns()}

        {children}
        {/*children is use for  custom column  like check box, button, text hyperlink etc */}
      </Grid>
    </div>
  );
};

export default GridComponent;
