import React, { useState } from 'react';
import { process } from '@progress/kendo-data-query';
import patientSearchData from './patientSearchData.json';

const GridCommonActions = (initialState, rows = patientSearchData) => {
  const createDataState = (rows, dataState) => {
    return {
      result: process(rows, dataState),
      dataState: dataState
    };
  };

  const [data, setData] = useState(createDataState(rows, initialState));

  const changeEventHandler = event => {
    setData(createDataState(rows, event.data));
  };

  //check box handler
  const chkChangeHandler = (e, rowItem) => {
    // console.log(rowItem);
    // alert('click');
    // rowItem.Discontinued = !rowItem.Discontinued;
    // rows.map(item => {
    //   if (item.ProductID == rowItem.ProductID) {
    //     return (item.Discontinued = rowItem.Discontinued);
    //   }
    // });
    // setData(createDataState(rows, data.dataState));
  };

  //custom menu functions start
  const handlerClaimClick = (e, item) => {
    console.log(e);
    console.log(item);
    alert('Claim btn click');
  };
  const handlerTransClick = (e, item) => {
    console.log(e);
    console.log(item);
    alert('Transaction btn click');
  };

  const handlerChkClick = (e, item) => {
    console.log(e);
    console.log(item);
    alert('chk click');
  };

  const handlerTextLink = (e, item) => {
    console.log(e);
    console.log(item);
    alert('link text click');
  };

  //custom menu functions end

  //Start Grid Columns  Array start
  const columns = [
    {
      //common things
      props: {
        field: '',
        title: 'Actions',
        width: '130px',
        filter: 'text',
        filterable: false
      },
      columnMenu: '',
      element: 'button2',
      events: [handlerClaimClick, handlerTransClick],
      btnTitle: ['C', 'T'],
      cssStyle: 'gridButtoStyle',
      customColumn: true,
      disabled: false
    },
    {
      props: {
        field: 'FullName',
        title: 'Full Name',
        width: '220px',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenuCheckboxFilter',
      element: 'textlink',
      events: [handlerTextLink],
      btnTitle: '',
      cssStyle: '',
      customColumn: true,
      disabled: false
    },
    {
      props: {
        field: 'AccountNo',
        title: 'Account No',
        width: '110px',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenu',
      element: '',
      events: [],
      btnTitle: '',
      cssStyle: '',
      customColumn: false,
      disabled: false
    },
    {
      props: {
        field: 'SSN',
        title: 'SSN',
        width: '110px',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenuCheckboxFilter',
      events: [],
      btnTitle: '',
      cssStyle: '',
      customColumn: false,
      disabled: false
    },
    {
      props: {
        field: 'DOB',
        title: 'DOB',
        width: '110px',
        filter: 'date',
        filterable: false
      },
      columnMenu: 'ColumnMenuCheckboxFilter',
      element: '',
      events: [],
      btnTitle: '',
      cssStyle: '',
      customColumn: false,
      disabled: false
    },
    {
      props: {
        field: 'ClientName',
        title: 'Client Name',
        width: '110px',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenuCheckboxFilter',
      element: '',

      events: [],
      btnTitle: '',
      cssStyle: '',
      disabled: false,
      customColumn: false
    },
    {
      props: {
        field: 'Payer',
        title: 'Payer',
        width: '200px',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenuCheckboxFilter',
      element: '',
      events: [],
      btnTitle: '',
      cssStyle: '',
      disabled: false,
      customColumn: false
    },
    {
      props: {
        field: 'Plan',
        title: 'Plan',
        width: '200px',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenuCheckboxFilter',
      element: '',
      events: [],
      btnTitle: '',
      cssStyle: '',
      disabled: false,
      customColumn: false
    },
    {
      props: {
        field: 'SubscriberID',
        title: 'Subscriber ID',
        width: '',
        filter: 'text',
        filterable: false
      },
      columnMenu: 'ColumnMenu',
      element: '',
      events: [],
      btnTitle: '',
      cssStyle: '',
      disabled: false,
      customColumn: false
    }
  ];

  //End Grid columns

  const gridProps = {
    columns: columns,
    gridClass: 'patientInquiryGrid',
    dataState: initialState,
    rows: data, // data={result:{data:[],total:77},dataState:{take:10,skip:0}}
    total: rows.length, // optional
    sortable: true, // sorting
    pageable: true, // pagination
    filterable: false, // grid level filterable
    dataCollection: rows,
    keyColumn: 'ProductID', // optional  key unique render
    changeEventHandler: changeEventHandler
  };
  return [gridProps, chkChangeHandler];
};

export default GridCommonActions;
