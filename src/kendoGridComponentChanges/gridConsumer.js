import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import '@progress/kendo-theme-material/dist/all.css';
import GridComponent from '../kendoGridComponentChanges/gridComponent';
import { GridColumn as CustomeColumn } from '@progress/kendo-react-grid';
import GridCommonActions from './gridCommonActions';
import './gridCustomCss.scss';

//Component started from here
const GridConsumer = props => {
  const [gridProps, chkChangeHandler] = GridCommonActions({
    take: 10,
    skip: 0
  }); // pass initialState and gridData GridCommonActions({ take: 10, skip: 0  },[]);

  const getCustomeColumn = key => {
    switch (key) {
      case 'checkbox':
        return (
          <CustomeColumn
            field='Discontinued'
            filter='boolean'
            filterable={false}
            cell={props => (
              <td>
                <input
                  type='checkbox'
                  checked={props.dataItem[props.field]}
                  onChange={e => {
                    chkChangeHandler(e, props.dataItem); // provided by Developer
                  }}
                />
              </td>
            )}
          />
        );
        break;
      case 'date':
        break;
      case 'labelUnderline':
        break;
      default:
        break;
    }
  };

  return (
    <div className='App'>
      <div
        style={{
          borderRadius: '05pt 05pt 0pt 0pt',
          border: '0.5px solid #ccc',
          padding: '5px 5px 5px 30px',
          fontWeight: 'bold'
        }}
      >
        <span>Patient Search Result</span>
        <span style={{ marginLeft: '845px' }}>
          <button
            style={{ height: '30px', borderRadius: '6pt', width: '141px' }}
          >
            Merge Patients
          </button>
          <button
            style={{
              height: '30px',
              borderRadius: '6pt',
              width: '141px',
              marginLeft: '5pt'
            }}
          >
            New Patient
          </button>
        </span>
      </div>
      <GridComponent {...gridProps}>
        {/* {getCustomeColumn('checkbox')}*/}
      </GridComponent>
    </div>
  );
};

GridConsumer.propTypes = {};

export default GridConsumer;
