import React from 'react';
import {
  GridColumnMenuFilter,
  GridColumnMenuSort,
  GridColumnMenuCheckboxFilter
} from '@progress/kendo-react-grid';

import { DropDownList } from '@progress/kendo-react-dropdowns';

export class ColumnMenu extends React.Component {
  render() {
    return (
      <div>
        <GridColumnMenuSort {...this.props} />
        <GridColumnMenuFilter {...this.props} />
      </div>
    );
  }
}

export function GridColumnMenuCheck(collection) {
  return class extends React.Component {
    render() {
      return (
        <div>
          <GridColumnMenuSort {...this.props} />
          <GridColumnMenuCheckboxFilter {...this.props} data={collection} />
        </div>
      );
    }
  };
}

export function dropdownFilterCell(data, defaultItem) {
  return class extends React.Component {
    render() {
      return (
        <div className='k-filtercell'>
          <DropDownList
            data={data}
            onChange={this.onChange}
            value={this.value || defaultItem}
            defaultItem={defaultItem}
          />
          <button
            className='k-button k-button-icon k-clear-button-visible'
            title='Clear'
            disabled={!this.hasValue(this.props.value)}
            onClick={this.onClearButtonClick}
          >
            <span className='k-icon k-i-filter-clear' />
          </button>
        </div>
      );
    }
    hasValue = value => Boolean(value && value !== defaultItem);

    onChange = event => {
      const hasValue = this.hasValue(event.target.value);
      debugger;
      this.props.onChange({
        value: hasValue ? event.target.value : '',
        operator: hasValue ? 'eq' : '',
        syntheticEvent: event.syntheticEvent
      });
    };

    onClearButtonClick = event => {
      event.preventDefault();
      this.props.onChange({
        value: '',
        operator: '',
        syntheticEvent: event
      });
    };
  };
}
