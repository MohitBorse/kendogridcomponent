import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import { process } from '@progress/kendo-data-query';
import './KendoGrid.scss';
import { ColumnMenu, ColumnMenuCheckboxFilter } from './ColumnMenuFilter.js';
import { filterBy } from '@progress/kendo-data-query';

const KendoGrid = ({
  gridColumn,
  children,
  gridClass,
  products,
  takeRows,
  skipRows,
  sortable,
  pageable,
  keyColumn
}) => {
  let Coulmnfields = null;

  const createDataState = dataState => {
    return {
      result: process(products.slice(0), dataState),
      dataState: dataState
    };
  };

  const [data, setData] = useState(
    createDataState({ take: takeRows, skip: skipRows })
  );

  const setGridOptions = dataItem => {
    return createDataState(dataItem);
  };

  const dataStateChange = event => {
    setData(createDataState(event.data));
  };

  let filterData = null;
  const getColumnFilter = name => {
    switch (name) {
      case 'ColumnMenu':
        return (filterData = ColumnMenu);
        break;
      case 'ColumnMenuCheckboxFilter':
        return (filterData = ColumnMenuCheckboxFilter);
        break;
      default:
        break;
    }
  };

  Coulmnfields = gridColumn.map((item, key) => {
    console.log(item.FlterType);
    if (!item.IsCustomColumn) {
      return (
        <Column
          key={keyColumn + key}
          field={item.FieldName}
          filter={item.FlterType}
          columnMenu={getColumnFilter(item.CoumnFilter)}
          filterable={item.Filterable}
          title={item.ColumnHeader}
          width={item.ColumnWidth}
        />
      );
    } else {
      return <Column field={item.FieldName} />;
    }
  });

  return (
    <div>
      <div>
        {/* filterable */}
        <Grid
          className={gridClass}
          data={data.result}
          {...data.dataState}
          onDataStateChange={dataStateChange}
          sortable={sortable}
          pageable={pageable}
          resizable
        >
          {Coulmnfields}
          {children}
        </Grid>
      </div>
    </div>
  );
};

export default KendoGrid;
