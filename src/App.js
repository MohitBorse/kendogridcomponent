import React from 'react';
import logo from './logo.svg';
import './App.css';
import KendoGrid from './KendoGrid/KendoGrid';
import { GridColumn as CustomeColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-material/dist/all.css';
import products from '../src/KendoGrid/products.json';
// import GridConsumer from './KendoGrid2/GridConsumer';
import GridConsumer3 from './KendoGrid3/KendoGrid3';
import GridComp from './KendoGrid3/GridComp';
import KendoGridConsumer from './kendoGridComponent/kendoGridConsumer';
import PanelBarConsumer from './panelBarComponent/panelBarConsumer';
import GridConsumer from './kendoGridComponentChanges/gridConsumer';
const gridColumn = [
  {
    FieldName: 'ProductID',
    ColumnHeader: 'ID',
    ColumnWidth: '40px',
    FlterType: 'numeric',
    CoumnFilter: 'ColumnMenu',
    IsCustomColumn: false,
    Filterable: false
  },
  {
    FieldName: 'ProductName',
    ColumnHeader: 'Name',
    ColumnWidth: '250px',
    FilterType: 'text', //date , boolean
    CoumnFilter: 'ColumnMenuCheckboxFilter',
    IsCustomColumn: false,
    Filterable: true
  }
];

const chkChangeHandler = (e, props) => {
  alert('check box handler');
  console.log(e.target);
  console.log(props);
};

const kendoProp = {
  gridColumn: gridColumn,
  gridClass: 'gridClass',
  products: products,
  takeRows: 10,
  skipRows: 0,
  sortable: true,
  pageable: true,
  keyColumn: 'ProductID'
};

function App() {
  return (
    <div className='App'>
      {/* <KendoGrid {...kendoProp}></KendoGrid> */}
      {/* <GridConsumer></GridConsumer> */}
      {/* <GridComp></GridComp> */}
      {/* <KendoGridConsumer></KendoGridConsumer>*/}
      {/* <PanelBarConsumer></PanelBarConsumer> */}
      <GridConsumer></GridConsumer>
    </div>
  );
}

export default App;
