import React from 'react';
import './panelBar.scss';
import { PanelBar, PanelBarItem } from '@progress/kendo-react-layout';

const panelBarComponent = ({
  handlePnaelBarSelect,
  panelBarList,
  panelWrapperClass
}) => {
  const getPanelBarItem = panelBarList.map((item, key) => {
    return (
      <PanelBarItem
        id={key}
        key={item.title}
        expanded={item.expanded}
        disabled={item.disabled}
        title={item.title}
        selected={item.selected}
      >
        {item.content}
      </PanelBarItem>
    );
  });
  return (
    <React.Fragment>
      <div className={panelWrapperClass}>
        <PanelBar onSelect={handlePnaelBarSelect}>{getPanelBarItem}</PanelBar>
      </div>
    </React.Fragment>
  );
};

export default panelBarComponent;
