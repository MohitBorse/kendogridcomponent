import React from 'react';
import PanelBarComponent from './panelBarComponent';
import panelBarComponentHook from './panelBarComponentHook';

const panelBarConsumer = () => {
  const [panelBarCollection, handlePnaelBarSelect] = panelBarComponentHook();

  const panelbarProps = {
    handlePnaelBarSelect: handlePnaelBarSelect,
    panelWrapperClass: 'panel-wrapper',
    panelBarList: panelBarCollection
  };
  return (
    <div>
      <PanelBarComponent {...panelbarProps}>haa</PanelBarComponent>
    </div>
  );
};

export default panelBarConsumer;
