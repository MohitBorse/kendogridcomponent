import React from 'react';

const panelBarComponentHook = () => {
  const handlePnaelBarSelect = e => {
    debugger;
    console.log(e.action);
    console.log(e.target.props.title);
  };

  const imageUrl = imageName => {
    let baseImageUrl =
      'https://demos.telerik.com/kendo-ui/content/web/panelbar/';
    return baseImageUrl + imageName + '.jpg';
  };

  const panelBarCollection = [
    {
      title: 'Referal',
      expanded: false,
      disabled: false,
      selected: true,
      content: (
        <div>
          <div className='teamMate'>
            <img src={imageUrl('andrew')} alt='Andrew Fuller' />
            <span className='mate-info'>
              <h2>Andrew Fuller</h2>
              <p>Team Lead</p>
            </span>
          </div>
          <div className='teamMate'>
            <img src={imageUrl('nancy')} alt='Nancy Leverling' />
            <span className='mate-info'>
              <h2>Nancy Leverling</h2>
              <p>Sales Associate</p>
            </span>
          </div>
          <div className='teamMate'>
            <img src={imageUrl('robert')} alt='Robert King' />
            <span className='mate-info'>
              <h2>Robert King</h2>
              <p>Business System Analyst</p>
            </span>
          </div>

          <div className='teamMate'>
            <img src={imageUrl('andrew')} alt='Andrew Fuller' />
            <span className='mate-info'>
              <h2>Andrew Fuller</h2>
              <p>Team Lead</p>
            </span>
          </div>
          <div className='teamMate'>
            <img src={imageUrl('nancy')} alt='Nancy Leverling' />
            <span className='mate-info'>
              <h2>Nancy Leverling</h2>
              <p>Sales Associate</p>
            </span>
          </div>
          <div className='teamMate'>
            <img src={imageUrl('robert')} alt='Robert King' />
            <span className='mate-info'>
              <h2>Robert King</h2>
              <p>Business System Analyst</p>
            </span>
          </div>
        </div>
      )
    },
    {
      title: 'KendoGrid',
      expanded: false,
      disabled: false,
      selected: false,
      content: <div class='custom-template'>First item content</div>
    }
  ];

  return [panelBarCollection, handlePnaelBarSelect];
};

export default panelBarComponentHook;
