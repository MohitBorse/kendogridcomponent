import React, { useState } from 'react';
import { process } from '@progress/kendo-data-query';
const createDataState = (products, dataState) => {
  return {
    result: process(products, dataState),
    dataState: dataState
  };
};

const useKendoGridHook = (products, defaultDataState, gridColumn) => {
  const [data, setData] = useState(createDataState(products, defaultDataState));
  const changeEventHandler = event => {
    setData(createDataState(products, event.data));
  };

  //check box handler
  const chkChangeHandler = (e, rowItem) => {
    console.log(rowItem);
    rowItem.Discontinued = !rowItem.Discontinued;
    products.map(item => {
      if (item.ProductID == rowItem.ProductID) {
        return (item.Discontinued = rowItem.Discontinued);
      }
    });
    setData(createDataState(products, data.dataState));
  };

  const kendoProp = {
    gridColumn: gridColumn,
    gridClass: 'gridClass',
    dataState: defaultDataState,
    products: data, // data={result:{data:[],total:77},dataState:{take:10,skip:0}}
    total: products.length, // optional
    sortable: true, // sorting
    pageable: true, // pagination
    filterable: true, // grid level filterable
    keyColumn: 'ProductID', // optional  key unique render
    changeEventHandler: changeEventHandler
  };
  return [kendoProp, chkChangeHandler, changeEventHandler, createDataState];
};

export default useKendoGridHook;
