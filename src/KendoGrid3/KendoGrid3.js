import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import { process } from '@progress/kendo-data-query';
import '../KendoGrid/KendoGrid.scss';
import { ColumnMenu, ColumnMenuCheckboxFilter } from './ColumnMenuFilter.js';
import { filterBy } from '@progress/kendo-data-query';

const KendoGrid3 = ({
  gridColumn, // column
  children, // custom column
  gridClass, //css
  products, // CollectionData
  dataState, //pagination state skip take
  sortable, // sortable
  pageable, //pageable
  filterable, //grid level fiter
  keyColumn, //

  changeEventHandler
}) => {
  let Coulmnfields = null;
  let filterData = null;

  const getColumnFilter = name => {
    switch (name) {
      case 'ColumnMenu':
        return (filterData = ColumnMenu);
        break;
      case 'ColumnMenuCheckboxFilter':
        return (filterData = ColumnMenuCheckboxFilter);
        break;
      default:
        break;
    }
  };

  const generateColumns = () => {
    return gridColumn.map((item, key) => {
      if (!item.IsCustomColumn) {
        return (
          <Column
            key={item}
            field={item.FieldName}
            filter={item.FlterType}
            columnMenu={getColumnFilter(item.CoumnFilter)}
            filterable={item.Filterable}
            title={item.ColumnHeader}
            width={item.ColumnWidth}
          />
        );
      }
    });
  };

  return (
    <div>
      <Grid
        className={gridClass}
        data={products.result}
        {...products.dataState}
        onDataStateChange={e => changeEventHandler(e)}
        sortable={sortable}
        filterable={filterable}
        pageable={pageable}
        resizable //Not working
      >
        {generateColumns()}
        {children}
        {/*children is use for  custom column  like check box, button, text hyperlink etc */}
      </Grid>
    </div>
  );
};

export default KendoGrid3;
