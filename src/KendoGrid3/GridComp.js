import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import KendoGrid from '../KendoGrid/KendoGrid';
import { GridColumn as CustomeColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-default/dist/all.css';
// import products from '../KendoGrid/product.json';
import products from './products.json';
import KendoGrid3 from '../KendoGrid3/KendoGrid3';
import { process } from '@progress/kendo-data-query';
import { DatePicker } from '@progress/kendo-react-dateinputs';
import { DropDownList } from '@progress/kendo-react-dropdowns';
import useKendoGridHook from './useKendoGridHook';

const gridColumn = [
  {
    FieldName: 'ProductID',
    ColumnHeader: 'ID',
    ColumnWidth: '160px',
    FlterType: 'numeric',
    CoumnFilter: 'ColumnMenu', // custome filter
    IsCustomColumn: false,
    Filterable: false // column level filterable
  },
  {
    FieldName: 'ProductName',
    ColumnHeader: 'Name',
    ColumnWidth: '250px',
    FilterType: 'text', //date , boolean
    CoumnFilter: 'ColumnMenuCheckboxFilter', // custome filter
    IsCustomColumn: false,
    Filterable: true
  }
];

const defaultDataState = { takeRows: 10, skipRows: 0 };

// const createDataState = (products, dataState) => {
//   return {
//     result: process(products, dataState),
//     dataState: dataState
//   };
// };

//Component started from here
const GridComp = props => {
  const [kendoProp, chkChangeHandler] = useKendoGridHook(
    products,
    defaultDataState,
    gridColumn
  );
  // const [data, setData] = useState(createDataState(products, defaultDataState));
  // const changeEventHandler = event => {
  //   setData(createDataState(products, event.data));
  // };

  //check box handler
  // const chkChangeHandler = (e, rowItem) => {
  //   console.log(rowItem);
  //   rowItem.Discontinued = !rowItem.Discontinued;
  //   products.map(item => {
  //     if (item.ProductID == rowItem.ProductID) {
  //       return (item.Discontinued = rowItem.Discontinued);
  //     }
  //   });
  //   setData(createDataState(products, data.dataState));
  // };

  // const kendoProp = {
  //   gridColumn: gridColumn,
  //   gridClass: 'gridClass',
  //   dataState: defaultDataState,
  //   products: data, // data={result:{data:[],total:77},dataState:{take:10,skip:0}}
  //   total: products.length, // optional
  //   sortable: true, // sorting
  //   pageable: true, // pagination
  //   filterable: true, // grid level filterable
  //   keyColumn: 'ProductID', // optional  key unique render
  //   changeEventHandler: changeEventHandler
  // };

  return (
    <div className='App'>
      <KendoGrid3 {...kendoProp}>
        <CustomeColumn
          field='Discontinued'
          filter='boolean'
          cell={props => (
            <td>
              <input
                type='checkbox'
                checked={props.dataItem[props.field]}
                onChange={e => {
                  chkChangeHandler(e, props.dataItem); // provided by Developer
                }}
              />
            </td>
          )}
        />
        <CustomeColumn
          title='date'
          filter='date'
          format='{0:d}'
          cell={props => (
            <td>
              <DatePicker defaultValue={new Date()} />
              <DropDownList></DropDownList>
            </td>
          )}
        />
      </KendoGrid3>
    </div>
  );
};

GridComp.propTypes = {};

export default GridComp;
