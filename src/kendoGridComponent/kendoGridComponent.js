import React from 'react';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import '../KendoGrid/KendoGrid.scss';
import { ColumnMenu, ColumnMenuCheckboxFilter } from './ColumnMenuFilter.js';
import { filterBy } from '@progress/kendo-data-query';

const kendoGridComponent = ({
  columns, // column
  children, // custom column
  gridClass, //css
  rows, // CollectionData
  dataState, //pagination state skip take
  sortable, // sortable
  pageable, //pageable
  filterable, //grid level fiter
  keyColumn, //

  changeEventHandler
}) => {
  const getColumnFilter = name => {
    switch (name) {
      case 'ColumnMenu':
        return ColumnMenu;
        break;
      case 'ColumnMenuCheckboxFilter':
        return ColumnMenuCheckboxFilter;
        break;
      default:
        break;
    }
  };

  const generateColumns = () => {
    return columns.map((item, key) => {
      if (!item.isCustomColumn) {
        return (
          <Column
            key={item}
            field={item.field}
            title={item.columnText}
            width={item.columnWidth}
            filter={item.filterDataType}
            columnMenu={getColumnFilter(item.columnFilter)}
            filterable={item.filterable}
          />
        );
      } else {
        //   <td dangerouslySetInnerHTML={{ __html: item.cell }}></td>
        return (
          <Column
            field={item.field}
            filter={item.filterDataType}
            cell={props => (
              <td>{generateHtmlElement('checkbox', props, item)}</td>
            )}
          />
        );
      }
    });
  };

  const generateHtmlElement = (key, props, item) => {
    debugger;
    let functItem = item.func;
    switch (key) {
      case 'checkbox':
        return <input type='checkbox' checked={props.dataItem[props.field]} />;
        break;

      default:
        break;
    }
  };

  return (
    <div>
      <Grid
        className={gridClass}
        data={rows.result} // {result,total}
        {...rows.dataState}
        onDataStateChange={e => changeEventHandler(e)}
        sortable={sortable}
        filterable={filterable}
        pageable={pageable}
        resizable //Not working
      >
        {generateColumns()}
        {children}
        {/*children is use for  custom column  like check box, button, text hyperlink etc */}
      </Grid>
    </div>
  );
};

export default kendoGridComponent;
