import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import '@progress/kendo-theme-material/dist/all.css';
import products from './products.json';
import KendoGridComponent from '../kendoGridComponent/kendoGridComponent';
import { GridColumn as CustomeColumn } from '@progress/kendo-react-grid';
import { DatePicker } from '@progress/kendo-react-dateinputs';
import { DropDownList } from '@progress/kendo-react-dropdowns';
import KendoGridCommonHook from './KendoGridCommonHook';

const columns = [
  {
    field: 'ProductID',
    columnText: 'ID',
    columnWidth: '160px',
    filterDataType: 'numeric',
    columnFilter: 'ColumnMenu', // custome filter
    isCustomColumn: false,
    filterable: false // column level filterable
  },
  {
    field: 'ProductName',
    columnText: 'Name',
    columnWidth: '250px',
    filterDataType: 'text', //date , boolean
    columnFilter: 'ColumnMenuCheckboxFilter', // custome filter
    isCustomColumn: false,
    filterable: true
  },
  {
    field: 'Category.CategoryName',
    columnText: 'CategoryName',
    filterDataType: 'text',
    isCustomColumn: false,
    filterable: true
  },
  {
    field: 'UnitPrice',
    columnText: 'Price',
    filterDataType: 'numeric',
    isCustomColumn: false,
    filterable: true
  },
  {
    field: 'UnitsInStock',
    columnText: 'In stock',
    filterDataType: 'numeric',
    isCustomColumn: false,
    filterable: true
  }

  //   {
  //     isCustomColumn: true,
  //     field: 'Discontinued'
  //   }
];

const rows = products; //DataCollection

//Component started from here
const kendoGridConsumer = props => {
  const [kendoProp, chkChangeHandler] = KendoGridCommonHook(columns, rows, {
    takeRows: 10,
    skipRows: 0
  });

  const labelHandlerClick = () => {
    alert('label click');
  };
  const getCustomeColumn = key => {
    switch (key) {
      case 'checkbox':
        return (
          <CustomeColumn
            field='Discontinued'
            filter='boolean'
            cell={props => (
              <td>
                <input
                  type='checkbox'
                  checked={props.dataItem[props.field]}
                  onChange={e => {
                    chkChangeHandler(e, props.dataItem); // provided by Developer
                  }}
                />
              </td>
            )}
          />
        );
        break;
      case 'date':
        return (
          <CustomeColumn
            title='date'
            filter='date'
            format='{0:d}'
            cell={props => (
              <td>
                <DatePicker defaultValue={new Date()} />
                <DropDownList></DropDownList>
              </td>
            )}
          />
        );
        break;
      case 'labelUnderline':
        return (
          <CustomeColumn
            title='Product Name'
            field='ProductName'
            filter='text'
            cell={props => (
              <td>
                <label
                  style={{ textDecoration: 'underline' }}
                  onClick={labelHandlerClick}
                >
                  {props.dataItem[props.field]}
                </label>
              </td>
            )}
          />
        );
        break;
      default:
        break;
    }
  };

  return (
    <div className='App'>
      <KendoGridComponent {...kendoProp}>
        {getCustomeColumn('checkbox')}
        {getCustomeColumn('date')}
        {getCustomeColumn('labelUnderline')}
      </KendoGridComponent>
    </div>
  );
};

kendoGridConsumer.propTypes = {};

export default kendoGridConsumer;
