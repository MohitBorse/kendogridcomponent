import React, { useState } from 'react';
import { process } from '@progress/kendo-data-query';

const createDataState = (rows, dataState) => {
  return {
    result: process(rows, dataState),
    dataState: dataState
  };
};

const KendoGridCommonHook = (columns, rows, initialState) => {
  const [data, setData] = useState(createDataState(rows, initialState));
  const changeEventHandler = event => {
    setData(createDataState(rows, event.data));
  };

  //check box handler
  const chkChangeHandler = (e, rowItem) => {
    console.log(rowItem);
    alert('click');
    rowItem.Discontinued = !rowItem.Discontinued;
    rows.map(item => {
      if (item.ProductID == rowItem.ProductID) {
        return (item.Discontinued = rowItem.Discontinued);
      }
    });
    setData(createDataState(rows, data.dataState));
  };

  const kendoProp = {
    columns: columns,
    gridClass: 'gridClass',
    dataState: initialState,
    rows: data, // data={result:{data:[],total:77},dataState:{take:10,skip:0}}
    total: rows.length, // optional
    sortable: true, // sorting
    pageable: true, // pagination
    filterable: true, // grid level filterable
    keyColumn: 'ProductID', // optional  key unique render
    changeEventHandler: changeEventHandler
  };
  return [kendoProp, chkChangeHandler, changeEventHandler, createDataState];
};

export default KendoGridCommonHook;
